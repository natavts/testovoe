var sortField = undefined;
var roles = {1: "Administrator", 2: "Technician", 3:"Manager", 4: "Supervisor"};

function checkErrors(data) {
  var errors = [];

  var makeError = function (field, message) {
    return {field: field, message: message};
  }

  if (typeof data.first_name !== 'string' || data.first_name.length < 3 || data.first_name.length > 15) {
    errors.push(makeError("first_name", "error first name"));
  }

  if (typeof data.last_name !== 'string' || data.last_name.length < 3 || data.last_name.length > 25) {
    errors.push(makeError("last_name", "error last name"));
  }

  if (typeof data.active !== 'boolean') {
    errors.push(makeError("active", "error active"));
  }

  if ((data.age ^ 0) !== data.age) {
    errors.push(makeError("age", "error age"));
  }

  var pattern=/^[a-zA-Z0-9_-]+$/;
  var matchStatus = !!pattern.exec(data.login);
  if (!matchStatus || !data.login) {
    errors.push(makeError("login", "error login"));
  }

  if (typeof data.password !== 'string' || data.password.length < 8) {
    errors.push(makeError("password", "error password"));
  }

  if ((data.role ^ 0) !== data.role || data.role < 1 || data.role > 4) {
    errors.push(makeError("role", "error role"));
  }

  return errors.length ? errors : null;
}

function showError(container, errorMessage) {
  container.className += ' error';
  var msgElem = document.createElement('p');
  msgElem.className = "text-danger";
  msgElem.innerHTML = errorMessage;
  container.appendChild(msgElem);
}

function resetError() {
  var errors = document.getElementsByClassName("form-group error");
  while (errors.length) {
    errors[0].removeChild(errors[0].lastChild);
    errors[0].className = 'form-group';
  }
}

var User = function (data) {
  this.first_name = data.first_name;
  this.last_name = data.last_name;
  this.full_name = data.first_name + ' ' + data.last_name;
  this.active = data.active;
  this.age = data.age;
  this.login = data.login;
  this.password = data.password;
  this.role = data.role;
  this.registered_on = +new Date();
}

function getFormData() {
  resetError();
  var elems = document.getElementById("form").elements;
  return {
    first_name: elems.first_name.value,
    last_name: elems.last_name.value,
    active: elems.active.checked,
    age: elems.age.value ? +elems.age.value : NaN,
    login: elems.login.value,
    password: elems.password.value,
    role: +elems.role.value,
  }
}

function createUser(form) {
  var elems = document.getElementById("form").elements;
  var data = getFormData();
  var user = new User(data);
  var errors = checkErrors(user);
  if(errors) {
    errors.forEach(function (err) {
      showError(elems[err.field].parentNode, err.message);
    })
  } else {
    collection.add(user);
    form.reset();
    sortField = undefined;
  };
}

function cancelEdit() {
  resetError();
  form.reset();
  document.getElementById('submit').style.display = "initial";
  document.getElementById('edit').style.display = "none";
  document.getElementById('cancel').style.display = "none";
}

function editUser(i) {
  resetError();
  var elems = document.getElementById("form").elements;
  var user = collection.get()[i]
  for (var prop in user) {
    if (document.getElementById(prop)) {
      document.getElementById(prop).value = user[prop];
    }
  }
  document.getElementById('active').checked = user.active;
  document.getElementById('submit').style.display = "none";
  document.getElementById('edit').style.display = "initial";
  document.getElementById('cancel').style.display = "initial";

  document.getElementById('edit').onclick = function() {
    var data = getFormData();
    var errors = checkErrors(data);
    if(errors) {
      errors.forEach(function (err) {
        showError(elems[err.field].parentNode, err.message);
      })
    } else {
      collection.edit(data, i);
      cancelEdit();
      sortField = undefined;
    };
  }
};

function sortByActive(a, b) {
  return (a.active === b.active)? 0 : a.active? -1 : 1;
};


function sortByField(fieldName) {
  return function (a, b) {
    if (a[fieldName] < b[fieldName])
     return -1;
    if (a[fieldName] > b[fieldName])
      return 1;
    return 0;
  }
}

function sortTable(fieldName) {
  if(sortField == fieldName) {
    collection.reverse().sort(sortByActive);
  } else {
    collection.sort(sortByField(fieldName)).sort(sortByActive)
  }
  sortField = fieldName;
  collection.fillTable();
}



var Collection = (function(){
  var users = [];
  function Collection() {};
  Collection.prototype = {
    constructor: Collection,
    get: function(){
      return users;
    },
    add: function (user) {
      var message;
      if (!(user instanceof User)) {
        message = {message: "user not User"};
      } else {
        var errors = checkErrors(user);
        if(!errors){
          users.push(user);
          this.fillTable();
        }
        message = {message: "user added"};
      }
      return message;
    },
    edit: function (data, i) {
      var errors = checkErrors(data);
      if(!errors){
        for (var prop in users[i]) {
          if (prop == "registered_on") continue;
          users[i][prop] = data[prop];
        }
        users[i].full_name = data.first_name + ' ' + data.last_name;
        this.fillTable();
      }
      return errors;
    },
    fillTable: function () {
      var el = document.getElementById('usersTable');
      var data = '';
      if (users.length > 0) {
        for (i = 0; i < users.length; i++) {
          var date = new Date(users[i].registered_on);
          var formatedDate = date.getDate() + '.' + (+date.getMonth()+1) + '.' + date.getFullYear() + ' '
           + date.getHours()+':'+date.getMinutes();
          data += '<tr>';
          data += '<td>' + roles[users[i].role] + '</td>';
          data += '<td>' + users[i].login + '</td>';
          data += '<td>' + users[i].full_name + '</td>';
          data += '<td>' + users[i].age + '</td>';
          data += '<td>' + formatedDate + '</td>';
          data += '<td class="' + (users[i].active ? 'user-active' : 'user-not-active') + '">' +
          (users[i].active ? 'Yes' : 'No') + '</td>';
          data += '<td>' + '<input class="btn btn-sm btn-default"' +
           'type="button" onclick="editUser(' + i + ');" value="edit"/>' + '</td>';
          data += '</tr>';
        }
     }
     return el.innerHTML = data;
   },
   forEach : function (cb) {
     return Array.prototype.forEach.call(users, cb);
   },
   sort: function (fn) {
     fn = fn || function (a, b) {
       if (a.login < b.login)
        return -1;
       if (a.login > b.login)
         return 1;
       return 0;
     }

     return Array.prototype.sort.call(users, fn);
   },
   reverse: function(){
     return Array.prototype.reverse.call(users);
   },
   map: function (cb) {
     return Array.prototype.map.call(users, cb);
   }
  }
  return Collection;
})();

var collection = new Collection();
